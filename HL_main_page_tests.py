import unittest
import time
from selenium import webdriver

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


class HealthLabsFrontPageTests(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=r'C:\Driver\chromedriver_win32\chromedriver.exe')
        self.base_url = 'https://healthlabs.pl/'

    def tearDown(self):
        self.driver.quit()

    def test_cookie_acceptance_displayed(self):
        acceptance_displayed_xpath = '//*[@id="__layout"]//*[contains(@class, "cookie")]'
        driver = self.driver

        driver.get(self.base_url)
        time.sleep(3)
        driver.find_element_by_xpath(acceptance_displayed_xpath)

    def test_cookie_acceptance_button_works(self):
        cookie_button_xpath = '//*[@id="__layout"]/div/div[3]/div/button'
        base_url = self.base_url
        driver = self.driver

        driver.get(base_url)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, cookie_button_xpath))).click()

    def test_main_page_title(self):
        driver = self.driver
        base_url = self.base_url
        expected_main_page_title = 'Strona główna | HealthLabs'

        driver.get(base_url)
        actual_title = driver.title
        print(f'Actual title of HealthLabs Main Page: {actual_title}')
        self.assertEqual(expected_main_page_title, actual_title,
                         f'Expected title of {base_url} differ from actual title.')

    def test_Poznaj_Nasze_Produkty_button(self):
        driver = self.driver
        base_url = self.base_url
        poznaj_button_xpath = '//*[contains(@class, "products-link")]'
        expected_url = "https://healthlabs.pl/produkty"

        driver.get(base_url)
        poznaj_button = driver.find_element_by_xpath(poznaj_button_xpath)
        poznaj_button_element = poznaj_button.get_attribute('href')
        self.assertEqual(expected_url, poznaj_button_element,
                         f"After click on button: '\Poznaj Nasze Produkty'\ redirected to wrong url: {poznaj_button_element}")

    def test_number_of_products_categories(self):
        driver = self.driver
        expected_number_of_categories = 4
        category_xpath = '//*[@id="__layout"]/div/div[2]/main/section[2]/a'

        driver.get(self.base_url)
        category_elements = driver.find_elements_by_xpath(category_xpath)
        actual_number_of_category = len(category_elements)
        self.assertEqual(expected_number_of_categories, actual_number_of_category,
                         f"Number of products categories differ for Main Page: {self.base_url} and equals {actual_number_of_category}")

    def test_products_price_in_pln_currency(self):
        driver = self.driver
        expected_product_price_currency = "PLN"
        products_price_xpath = '//*[contains(@class, "bottom_price")]'

        driver.get(self.base_url)
        products_price_elements = driver.find_elements_by_xpath(products_price_xpath)

        for products_price_element in products_price_elements:
            products_price_element_text = products_price_element.get_attribute("textContent")
            with self.subTest(products_price_element_text):
                self.assertIn(expected_product_price_currency, products_price_element_text,
                              f"Products on Main Page does not contain expected price currency like is PLN, but {products_price_element_text} currency.")

    def test_number_of_sections_unfolds_in_hamburger_menu(self):
        driver = self.driver
        expected_number_of_sections = 6
        hamburger_button_xpath = '//*[@id="__layout"]/div/div[2]/header/div[1]/div[1]/button'
        section_xpath = '//*[@id="__layout"]/div/div[2]/header/div[2]/div/nav/div/a/span'

        driver.get(self.base_url)
        hamburger_button = driver.find_element_by_xpath(hamburger_button_xpath)
        hamburger_button.click()
        time.sleep(3)
        section_elements = driver.find_elements_by_xpath(section_xpath)
        actual_number_of_section = len(section_elements)

        self.assertEqual(expected_number_of_sections, actual_number_of_section,
                          f"Number of sections in Hamburger Menu on Main Page: {self.base_url} differ "
                          f"and equals {actual_number_of_section} but expected is {expected_number_of_sections}.")

