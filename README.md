# Portfolio

## About Me
My name is Mariola Szczurek.

I am a Master of Science in Chemistry and Chemical Technology, specializing in Cosmetics Technology at the Cracow University of Technology.

The year 2020 was a breakthrough for me, because then I decided to change my current professional direction - from the Cosmetic Technology industry to the Information Technology industry.

In my previous work, I took care of the highest quality of the cosmetic product at the initial stage of design and creation. I dealt with the quality control of finished products in production, ensuring that the finished products meet the requirements and specifications.
In the future, I would like to assure you that the software produced meets the requirements and is of the highest quality.

**By creating this portfolio, I would like to share all my knowledge and experience gained during self-study in the field of Software Testing.** 

I systematically learn new topics, techniques and tools, so I will try to add new projects and update those already added.

## Tools, languages and frameworks
  * [Screencast-O-Matic](https://screencast-o-matic.com/) - screencast and video editing tool
  * [XRecorder](https://screen-recorder-inshot-inc.pl.aptoide.com/app) - screen recorder on mobile device
  * [Bug Magnet](https://bugmagnet.org/) - exploratory testing assistant
  * [TestLink](https://testlink.org/) - online test management system
  * Python - *basic level*
  * [PyCharm](https://www.jetbrains.com/pycharm/) - IDE for Python
  * [Selenium WebDriver](https://www.selenium.dev/documentation/en/getting_started/quick/) - automatic testing of web applications *(basic level)*
  * [xPath Finder](https://chrome.google.com/webstore/detail/xpath-finder/ihnknokegkbpmofmafnkoadfjkhlogph) - plugin to get the elements xPath
  * [Postman](https://www.postman.com/) - REST API testing tool *(basic level)*
  * [Jira](https://www.atlassian.com/pl/software/jira) - project management and bug tracking tool
  * [GitLab](https://about.gitlab.com/) - open-source, a hosting website for programming projects based on a version control system

## My projects

* ### Testing web store HealthLabs.pl
       
    * [Test Plan Design Report](https://drive.google.com/file/d/1jZykUAzHUjQiEO-Bo9O90HdfPxSp9eGH/view?usp=sharing)
    * [Requirements Specification](https://drive.google.com/file/d/1UovZdErsEDiAOpd-XDA0hE3c2tHZajQj/view?usp=sharing)
    * [Test Suite and Test Cases](https://drive.google.com/file/d/1nuyYXxeSJ9rIsrOIUJ9TpIfuJZoby8Fq/view?usp=sharing)
    * [Test Execution Report](https://drive.google.com/file/d/1dCgVx0tIvPUDgj3W-Uz8nuZbxdSly1iD/view?usp=sharing)
    * [General Test Plan Metrics](https://drive.google.com/file/d/1YiTIQ0A84kTjZTkGk4yB0MaeMn8mEgTe/view?usp=sharing)
    * [Test Results - detailed, ver 1](https://drive.google.com/file/d/1eFXBD1_aI9pa_YkxClEA8F3GrnIomvwA/view?usp=sharing)
    * [Test Results - detailed, ver 2](https://drive.google.com/file/d/1K6tqAqnjETAwe4gdn9goWAazeb4MC0BZ/view?usp=sharing)
    * [Graphical Reports](https://drive.google.com/file/d/16SGujUWXVTTUIhAGGc82sDS4hRMMKvcU/view?usp=sharing)
    * Bug reports:
      * [ ID-1.0 - Windows 10 - Chrome 91.x - Registration with invalid first name and/or last name is possible](https://drive.google.com/file/d/1fcxV26Ug7JCoWyCUvfcUZZgGfY6iVMz3/view?usp=sharing) --- [[Photo_ID-1.0](https://drive.google.com/file/d/1yWnF6rXEuJvyYL93vQZ_RmTFom1I4VUT/view?usp=sharing)]
      * [ ID-1.1 - Windows 10 - Chrome 91.x - Registration with invalid name of street and/or apartment number is possible](https://drive.google.com/file/d/1hwGErJV8WXCACM8NPRS37T8TQzst6N15/view?usp=sharing) --- [[Photo_ID-1.1-(1)](https://drive.google.com/file/d/1mm9JVteeOgbNLg5vHIfQMdexWyoXrvQz/view?usp=sharing)], [[Photo_ID-1.1-(2)](https://drive.google.com/file/d/1oRCI1YWgxY4hyvMSI8fUpoDBbubvlXyG/view?usp=sharing)]
      * [ ID-1.2 - Windows 10 - Chrome 91.x - Registration with invalid city name is possible](https://drive.google.com/file/d/13_4orQF6I5v-iWl1-tlwfrA9riozxUoK/view?usp=sharing) --- [[Photo_ID-1.2-(1)](https://drive.google.com/file/d/1oDteAeeJ6IPzTcJIF4Wr1NnLCRJYi3dy/view?usp=sharing)], [[Photo_ID-1.2-(2)](https://drive.google.com/file/d/1Gq8wEpUqzsuW_JG2ieZHMmfNEvkr1KGu/view?usp=sharing)]
      * [ ID-3.1 - Windows 10 - Chrome 91.x - After click on Search field, search history is not displayed](https://drive.google.com/file/d/1ItfZwVmZP96_UUYdlsXJKEXpFHMarRc4/view?usp=sharing) --- [[Movie_ID-3.1](https://drive.google.com/file/d/1K1AjJm5tp72J3SkcckSggd_iJsUDiufF/view?usp=sharing)]
    
<br/>

* ### Testing with Selenium WebDriver
  
  * [HealthLabs.pl - main page tests](HL_main_page_tests.py)
    * [Allure report](Allure%20Report.pdf)
    * [Allure suites](allure_suites.csv)

 
<br/>
  
* ### Testing with Postman

  *In progress...*
  

## Courses

  * [The Complete 2021 Software Testing Bootcamp](https://www.udemy.com/course/testerbootcamp/)
  * [Academy of test.io](https://academy.test.io/en/)
  * [Python dla początkujących](https://www.udemy.com/course/python-dla-poczatkujacych/)
  * [Programowanie w języku Python - od A do Z - 2021](https://www.udemy.com/course/programowanie-w-jezyku-python/)
  * [Podstawy Testów Automatycznych w Selenium i Python, poziom podstawowy](https://jaktestowac.pl/course/pt1-mk1-podstawy-testow-automatycznych-w-selenium-i-python/)
  * [Podstawy Testów Automatycznych w Selenium i Python, poziom rozszerzony](https://jaktestowac.pl/course/pt1-mk5-podstawy-testow-automatycznych-w-selenium-i-python/) (*in progress*)
  * [Git dla testerów](https://jaktestowac.pl/course/gdt1-git-dla-testerow/)
  * [XPath - Zadania](https://jaktestowac.pl/xpath-kurs/)
  * [Postman: The Complete Guide - REST API Testing](https://www.udemy.com/course/postman-the-complete-guide/) *(in progress)*
  * [The Complete SQL Bootcamp 2021: Go from Zero to Hero](https://www.udemy.com/course/the-complete-sql-bootcamp/) *(in progress)*
  * [The Ultimate MySQL Bootcamp: Go from SQL Beginner to Expert](https://www.udemy.com/course/the-ultimate-mysql-bootcamp-go-from-sql-beginner-to-expert/) *(in progress)*
  * [Appium - Mobile App Automation in Python (Basics + Advance)](https://www.udemy.com/course/appium-with-python-tutorial/) *(to do)*
  * [Python dla średniozaawansowanych](https://www.udemy.com/course/python-dla-srednio-zaawansowanych/) *(to do)*

## Books

  * [Piotr Wicherski - Testowanie Oprogramowania](https://pwicherski.gitbook.io/testowanie-oprogramowania/)
  * Radosław Smilgin - Zawód tester, Wydanie I 
  * Adam Roman - Testowanie i jakość oprogramowania. Wydanie II rozszerzone (*in progress*)
  * Krzysztof Jadczyk - Pasja testowania. Wydanie II rozszerzone (*in progress*)
  * Aleksandra Kunysz - Kierunek jakość. Jak unikać błędów w projekcie (*in progress*)

## Blogs and websites

  * [wyszkolewas.pl - Waldemas Szafraniec Testy i Szkolenia](https://www.wyszkolewas.com.pl/)
  * [testerzy.pl](https://testerzy.pl/)
  * [toniebug.pl](https://www.toniebug.pl/)
  * [Remigiusz Bednarczyk - Dowiedz się, jak zostać testerem](https://remigiuszbednarczyk.pl/)
  * [testerka.pl](http://testerka.pl/)
  * [qa-courses.com](https://qa-courses.com/)
  * [cherry-it.pl/](http://cherry-it.pl/)


## Youtube channels

  * [Testuj.pl](https://www.youtube.com/channel/UC5nfCVMCEhYjCgnUoufoLhw)
  * [Zawód Tester](https://www.youtube.com/channel/UCUJzan4zBUpWwS1yWZZCwUw)
  * [testerzy.pl](https://www.youtube.com/user/testerzy)
  * [SamurajProgramowania](https://www.youtube.com/c/SamurajProgramowania)
  * [Mirosław Zelent - Pasja inormatyki](https://www.youtube.com/user/MiroslawZelent)
  * [nieinformatyk](https://www.youtube.com/c/nieinformatyk)
  * [QARANTANNA](https://www.youtube.com/channel/UC2TW4uIO9ayvdwTATBtBWKA)
  * [Stowarzyszenie Jakości Systemów Informatycznych](https://www.youtube.com/channel/UCm8WLD1_Pj0lhasiVEOGC6A)
